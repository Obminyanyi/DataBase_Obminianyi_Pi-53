CREATE PROCEDURE proc_del_reiting
@param1 as varchar(40)
as
DELETE FROM reiting
where Kod_student=(SELECT kod_st from student where SName=@param1)

execute proc_del_reiting @param1='Sname2'


CREATE PROCEDURE proc_del_para
@param1 as varchar(40)
as
DELETE FROM para
where K_pr=(SELECT K_pr from predmet where Nazva=@param1)

execute proc_del_para @param1='Predmet1'


update reiting set Reiting=Reiting+(Reiting*0.15)
where K_zapis=(select K_zapis 
from para p inner join v_zanyat v_z 
on p.K_lesson=v_z.K_lesson 
where V_lesson='pr')


update reiting set Reiting=Reiting-(Reiting*0.15)
where K_zapis=(select K_zapis 
from para p inner join v_zanyat v_z 
on p.K_lesson=v_z.K_lesson 
where V_lesson='lr')


alter procedure proc_insert_reiting
@param1 int,
@param2 real,
@param3 int
as
insert into reiting(K_zapis,Reiting,Kod_student)
select @param1,@param2,Kod_st from student where Kod_gr=@param3

exec proc_insert_reiting 18, 4, 3


insert into para(K_zapis, K_pr)
values(19,(select K_pr 
from predmet
where Nazva like ('%2')));

update para set N_modul=N_modul+1
where K_pr=(select K_pr 
from predmet
where Nazva='Predmet1');

CREATE PROCEDURE proc_del_stud
@param1 as int
as
DELETE FROM student
where Kod_gr=@param1

execute proc_del_para @param1=1;


create procedure proc_insert_reiting_two
@param1 int,
@param2 bit,
@param3 int
as
insert into reiting(K_zapis,Prisutnist,Kod_student)
select @param1,@param2,Kod_st from student where Kod_gr=@param3

exec proc_insert_reiting_two 17, 1, 3

update reiting set Prisutnist='True'
where Kod_student IS NOT NULL;

