﻿
--1)

SELECT sysobjects.name AS Таблица, sysindexes.name AS Индекс, sysindexes.indid AS Номер
FROM sysobjects INNER JOIN
 sysindexes ON sysobjects.id = sysindexes.id
WHERE (sysobjects.xtype = 'U') AND (sysindexes.indid > 0)
ORDER BY sysobjects.name, sysindexes.indid


select OBJECT_NAME(object_id) as table_name,
name as index_name, type, type_desc
from sys.indexes
where OBJECT_ID = OBJECT_ID(N'test_index')

create table test_index(
id int not null,
pole1 char(36) not null,
pole2 char(216) not null
)
select OBJECT_NAME(object_id) as table_name,
name as index_name, type, type_desc
from sys.indexes
where OBJECT_ID = OBJECT_ID(N'test_index')

select index_type_desc, page_count,
record_count,avg_page_space_used_in_persent
from sys.dm_db_index_physical_stats
(db_id(N'test_index_db'), OBJECT_ID(N'test_index_db'),Null,
Null,'Detailed')

exec dbo.sp_spaceused @objname=N'test_index',@updateusage = true;

insert into test_index
values(1, 'a','b')

declare @i as int=31
while @i<240
begin
	set @i = @i+1;
	insert into test_index
		values(@i, 'a','b')
	end;

insert into test_index
values(31, 'a','b')

declare @i as int=31
while @i<240
begin
	set @i = @i+1;
	insert into test_index
		values(@i, 'a','b')
	end;

select index_type_desc,page_count,
record_count,avg_page_space_used_in_percent
from sys.dm_db_index_physical_stats
(db_id(N'test_index_db'),OBJECT_ID(N'test_index_db'),Null,
Null,'Detailed')

exec dbo.sp_spaceused @objname=N'test_index',@updateusage=true;

insert into test_index
values(241, 'a','b')

truncate table test_index

create clustered index idx_cl_id on test_index(id)

declare @i as int=0
while @i<18630
begin
	set @i = @i+1;
	insert into test_index
		values(@i, 'a','b')
	end;

select index_type_desc,  index_depth, index_level, page_count,
record_count, avg_page_space_used_in_percent, 
	avg_fragmentation_in_percent
from sys.dm_db_index_physical_stats
(db_id(N'test_index_db'), OBJECT_ID(N'dbo.test_index'), Null,
 Null, 'Detailed')

exec dbo.sp_spaceused @objname = N'test_index', @updateusage = true;

insert into test_index
		values(18631, 'a','b') 

truncate table test_index
declare @i as int=0
while @i<8906
begin
	set @i = @i+1;
	insert into test_index
		values(@i%100, 'a','b')
	end;


insert into test_index
		values(8909%100, 'a','b')


truncate table test_index
drop index idx_cl_id on test_index
create clustered index idx_cl_pole1 on test_index(pole1)

declare @i as int=0
while @i<9000
begin
	set @i = @i+1;
	insert into test_index
		values(@i, format(@i, '0000'),'b')
	end;

	truncate table test_index
declare @i as int=0
while @i<9000
begin
	set @i = @i+1;
	insert into test_index
		values(@i, cast(newid() as char(36)),'b')
	end;

-- 1-2)

--індекс на кучі 
 drop index idx_cl_pole1 on test_index
 
 create nonclustered index idx_ncl_pole1 on test_index(pole1)
 truncate table test_index

declare @i as int=0
while @i<24472
begin
	set @i = @i+1;
	insert into test_index
		values(@i, format(@i, '0000'),'b')
	end;

insert into test_index
		values(24473, '000024473','b')


drop index idx_cl_id on test_index

 create clustered index idx_cl_pid on test_index(id)
 create nonclustered index idx_ncl_pole1 on test_index(pole1)
 truncate table test_index

declare @i as int=0
while @i<28864
begin
	set @i = @i+1;
	insert into test_index
		values(@i, format(@i, '0000'),'b')
	end;
	



	insert into test_index
		values(28865, '000028865','b')


	select * from test_index

	select index_type_desc,  index_depth, index_level, page_count,
record_count, avg_page_space_used_in_percent, 
	avg_fragmentation_in_percent
from sys.dm_db_index_physical_stats
(db_id(N'test1'), OBJECT_ID(N'dbo.test_index'), Null,
 Null, 'Detailed')

 alter index idx_ncl_pole1 on test_index rebuild
 alter index idx_cl_pid on test_index rebuild
	
 

select * from sys.indexes



--2) 
create nonclustered index id_date_rozm on zakaz(date_rozm)
with  fillfactor  = 70

select OBJECT_NAME(object_id) as table_name,
name as index_name, type, type_desc
from sys.indexes
where OBJECT_ID = OBJECT_ID(N'zakaz')

--3)
drop index id_klient_nazva_city on klient
create nonclustered index id_klient_nazva_city on klient(city, nazva)

--4)
select index_type_desc,  page_count,
record_count, avg_page_space_used_in_percent, avg_fragment_size_in_pages
from sys.dm_db_index_physical_stats
(db_id(N'torg_firm'), OBJECT_ID(N'klient'), Null,
 Null, 'Detailed')

--5)
alter table zakaz drop constraint FK__zakaz__id_sotrudnik__2D27B809
alter table sotrudnik drop constraint PK__sotrudnik__668829F13DE3C28F

--6)
SELECT id_tovar, Nazva, price FROM tovar WHERE Nazva = 'Moloko' 
SELECT id_tovar, Nazva, price FROM tovar WHERE Nazva = 'Молоко' AND Price = 10 
SELECT nazva FROM zakaz_tovar, tovar WHERE tovar.id_tovar = tovar.id_tovar
SELECT nazva FROM zakaz_tovar, tovar WHERE tovar.id_tovar = tovar.id_tovar
AND Price >10


alter proc myProc1
@param1 datetime,
@param2 datetime
AS
Select s.FIO,AVG(r.Reiting)
from student s INNER JOIN Groupa g
ON s.Kod_gr=g.Kod_gr INNER JOIN para p
ON g.Kod_gr=p.Kod_gr INNER JOIN reiting r
ON p.K_zapis=r.K_zapis 
where s.kod_st=r.kod_student --and p.Date > @param1 and p.Date < @param2--
GROUP BY s.FIO

exec myProc1 '12.12.12','12.12.12'

 


create proc myProc2
@param int
AS
Select s.FIO,AVG(r.Reiting)
from student s INNER JOIN Groupa g
ON s.Kod_gr=g.Kod_gr INNER JOIN para p
ON g.Kod_gr=p.Kod_gr INNER JOIN reiting r
ON p.K_zapis=r.K_zapis 
where s.kod_st=r.kod_student
GROUP BY s.FIO
having AVG(r.Reiting)>@param

exec myProc2 3;

 


create proc Myproc3 
AS
select sumStudas.FIO, sumStudas.Nazva,SUM(sumStudas.SumSt)
from (SELECT s.FIO, g.Kod_gr,pr.Nazva,r.Reiting,Sum(r.Reiting) as SumSt
FROM student s INNER JOIN Groupa g
ON s.Kod_gr=g.Kod_gr INNER JOIN para p
ON g.Kod_gr=p.Kod_gr INNER JOIN reiting r
ON p.K_zapis=r.K_zapis inner join predmet pr on pr.K_pr=p.K_pr
where s.kod_st=r.kod_student
GROUP BY s.FIO, g.Kod_gr,pr.Nazva,r.Reiting) as sumStudas
group by sumStudas.FIO, sumStudas.Nazva

exec Myproc3

 


CREATE TRIGGER MyTrig2 ON student FOR insert AS
BEGIN 
IF EXISTS (select * from inserted)
	BEGIN
		IF NOT EXISTS(select * from student inner join inserted on student.Kod_gr=inserted.Kod_gr)
		BEGIN
			insert into Groupa(Kod_gr) values((select Kod_gr from inserted))
		END
	END	
END


CREATE TRIGGER MyTrig1 ON student FOR delete AS
BEGIN 
IF EXISTS (select * from deleted)
	BEGIN
		IF NOT EXISTS(select * from student inner join deleted on student.Kod_gr=deleted.Kod_gr)
		BEGIN
			delete from Groupa where Kod_gr=(select Kod_gr from deleted)
		END
	END	
END
