
CREATE TABLE [Deadlines]
( 
	[idProjectName]      integer  NULL ,
	[ExpirationDate]     datetime  NULL 
)
go

CREATE TABLE [JobDone]
( 
	[idProjectName]      integer  NULL ,
	[idFullName]         integer  NULL ,
	[idWork]             integer  NULL ,
	[JobDoneDate]        datetime  NULL 
)
go

CREATE TABLE [Personal]
( 
	[idFullName]         integer  NOT NULL ,
	[FullName]           varchar(20)  NULL ,
	[EmploymentTheProject] varchar(20)  NULL 
)
go

ALTER TABLE [Personal]
	ADD CONSTRAINT [XPKPersonal] PRIMARY KEY  CLUSTERED ([idFullName] ASC)
go

CREATE TABLE [Project_Status]
( 
	[Status]             varchar(20)  NULL ,
	[Stage]              varchar(20)  NULL ,
	[idProjectName]      integer  NULL 
)
go

CREATE TABLE [Projects]
( 
	[idProjectName]      integer  NOT NULL ,
	[Cost]               integer  NULL ,
	[idFullName]         integer  NULL 
)
go

ALTER TABLE [Projects]
	ADD CONSTRAINT [XPKProjects] PRIMARY KEY  CLUSTERED ([idProjectName] ASC)
go

CREATE TABLE [Time_Specification]
( 
	[idWork]             integer  NOT NULL ,
	[WorkName]           varchar(20)  NULL ,
	[Specification]      datetime  NULL ,
	[TimeCost]           integer  NULL ,
	[idProjectName]      integer  NULL 
)
go

ALTER TABLE [Time_Specification]
	ADD CONSTRAINT [XPKTime_Specification] PRIMARY KEY  CLUSTERED ([idWork] ASC)
go


ALTER TABLE [Deadlines]
	ADD CONSTRAINT [R_6] FOREIGN KEY ([idProjectName]) REFERENCES [Projects]([idProjectName])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go


ALTER TABLE [JobDone]
	ADD CONSTRAINT [R_8] FOREIGN KEY ([idWork]) REFERENCES [Time_Specification]([idWork])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go


ALTER TABLE [Project_Status]
	ADD CONSTRAINT [R_5] FOREIGN KEY ([idProjectName]) REFERENCES [Projects]([idProjectName])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go


ALTER TABLE [Projects]
	ADD CONSTRAINT [R_4] FOREIGN KEY ([idFullName]) REFERENCES [Personal]([idFullName])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go


ALTER TABLE [Time_Specification]
	ADD CONSTRAINT [R_9] FOREIGN KEY ([idProjectName]) REFERENCES [Projects]([idProjectName])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go


CREATE TRIGGER tD_Deadlines ON Deadlines FOR DELETE AS
/* erwin Builtin Trigger */
/* DELETE trigger on Deadlines */
BEGIN
  DECLARE  @errno   int,
           @severity int,
           @state    int,
           @errmsg  varchar(255)
    /* erwin Builtin Trigger */
    /* Projects  Deadlines on child delete no action */
    /* ERWIN_RELATION:CHECKSUM="000150d1", PARENT_OWNER="", PARENT_TABLE="Projects"
    CHILD_OWNER="", CHILD_TABLE="Deadlines"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_6", FK_COLUMNS="idProjectName" */
    IF EXISTS (SELECT * FROM deleted,Projects
      WHERE
        /* %JoinFKPK(deleted,Projects," = "," AND") */
        deleted.idProjectName = Projects.idProjectName AND
        NOT EXISTS (
          SELECT * FROM Deadlines
          WHERE
            /* %JoinFKPK(Deadlines,Projects," = "," AND") */
            Deadlines.idProjectName = Projects.idProjectName
        )
    )
    BEGIN
      SELECT @errno  = 30010,
             @errmsg = 'Cannot delete last Deadlines because Projects exists.'
      GOTO error
    END


    /* erwin Builtin Trigger */
    RETURN
error:
   RAISERROR (@errmsg, -- Message text.
              @severity, -- Severity (0~25).
              @state) -- State (0~255).
    rollback transaction
END

go


CREATE TRIGGER tU_Deadlines ON Deadlines FOR UPDATE AS
/* erwin Builtin Trigger */
/* UPDATE trigger on Deadlines */
BEGIN
  DECLARE  @numrows int,
           @nullcnt int,
           @validcnt int,
           
           @errno   int,
           @severity int,
           @state    int,
           @errmsg  varchar(255)

  SELECT @numrows = @@rowcount
  /* erwin Builtin Trigger */
  /* Projects  Deadlines on child update no action */
  /* ERWIN_RELATION:CHECKSUM="00019103", PARENT_OWNER="", PARENT_TABLE="Projects"
    CHILD_OWNER="", CHILD_TABLE="Deadlines"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_6", FK_COLUMNS="idProjectName" */
  IF
    /* %ChildFK(" OR",UPDATE) */
    UPDATE(idProjectName)
  BEGIN
    SELECT @nullcnt = 0
    SELECT @validcnt = count(*)
      FROM inserted,Projects
        WHERE
          /* %JoinFKPK(inserted,Projects) */
          inserted.idProjectName = Projects.idProjectName
    /* %NotnullFK(inserted," IS NULL","select @nullcnt = count(*) from inserted where"," AND") */
    select @nullcnt = count(*) from inserted where
      inserted.idProjectName IS NULL
    IF @validcnt + @nullcnt != @numrows
    BEGIN
      SELECT @errno  = 30007,
             @errmsg = 'Cannot update Deadlines because Projects does not exist.'
      GOTO error
    END
  END


  /* erwin Builtin Trigger */
  RETURN
error:
   RAISERROR (@errmsg, -- Message text.
              @severity, -- Severity (0~25).
              @state) -- State (0~255).
    rollback transaction
END

go




CREATE TRIGGER tD_JobDone ON JobDone FOR DELETE AS
/* erwin Builtin Trigger */
/* DELETE trigger on JobDone */
BEGIN
  DECLARE  @errno   int,
           @severity int,
           @state    int,
           @errmsg  varchar(255)
    /* erwin Builtin Trigger */
    /* Time_Specification  JobDone on child delete no action */
    /* ERWIN_RELATION:CHECKSUM="00016356", PARENT_OWNER="", PARENT_TABLE="Time_Specification"
    CHILD_OWNER="", CHILD_TABLE="JobDone"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_8", FK_COLUMNS="idWork" */
    IF EXISTS (SELECT * FROM deleted,Time_Specification
      WHERE
        /* %JoinFKPK(deleted,Time_Specification," = "," AND") */
        deleted.idWork = Time_Specification.idWork AND
        NOT EXISTS (
          SELECT * FROM JobDone
          WHERE
            /* %JoinFKPK(JobDone,Time_Specification," = "," AND") */
            JobDone.idWork = Time_Specification.idWork
        )
    )
    BEGIN
      SELECT @errno  = 30010,
             @errmsg = 'Cannot delete last JobDone because Time_Specification exists.'
      GOTO error
    END


    /* erwin Builtin Trigger */
    RETURN
error:
   RAISERROR (@errmsg, -- Message text.
              @severity, -- Severity (0~25).
              @state) -- State (0~255).
    rollback transaction
END

go


CREATE TRIGGER tU_JobDone ON JobDone FOR UPDATE AS
/* erwin Builtin Trigger */
/* UPDATE trigger on JobDone */
BEGIN
  DECLARE  @numrows int,
           @nullcnt int,
           @validcnt int,
           
           @errno   int,
           @severity int,
           @state    int,
           @errmsg  varchar(255)

  SELECT @numrows = @@rowcount
  /* erwin Builtin Trigger */
  /* Time_Specification  JobDone on child update no action */
  /* ERWIN_RELATION:CHECKSUM="0001a39a", PARENT_OWNER="", PARENT_TABLE="Time_Specification"
    CHILD_OWNER="", CHILD_TABLE="JobDone"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_8", FK_COLUMNS="idWork" */
  IF
    /* %ChildFK(" OR",UPDATE) */
    UPDATE(idWork)
  BEGIN
    SELECT @nullcnt = 0
    SELECT @validcnt = count(*)
      FROM inserted,Time_Specification
        WHERE
          /* %JoinFKPK(inserted,Time_Specification) */
          inserted.idWork = Time_Specification.idWork
    /* %NotnullFK(inserted," IS NULL","select @nullcnt = count(*) from inserted where"," AND") */
    select @nullcnt = count(*) from inserted where
      inserted.idWork IS NULL
    IF @validcnt + @nullcnt != @numrows
    BEGIN
      SELECT @errno  = 30007,
             @errmsg = 'Cannot update JobDone because Time_Specification does not exist.'
      GOTO error
    END
  END


  /* erwin Builtin Trigger */
  RETURN
error:
   RAISERROR (@errmsg, -- Message text.
              @severity, -- Severity (0~25).
              @state) -- State (0~255).
    rollback transaction
END

go




CREATE TRIGGER tD_Personal ON Personal FOR DELETE AS
/* erwin Builtin Trigger */
/* DELETE trigger on Personal */
BEGIN
  DECLARE  @errno   int,
           @severity int,
           @state    int,
           @errmsg  varchar(255)
    /* erwin Builtin Trigger */
    /* Personal  Projects on parent delete no action */
    /* ERWIN_RELATION:CHECKSUM="00010345", PARENT_OWNER="", PARENT_TABLE="Personal"
    CHILD_OWNER="", CHILD_TABLE="Projects"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_4", FK_COLUMNS="idFullName" */
    IF EXISTS (
      SELECT * FROM deleted,Projects
      WHERE
        /*  %JoinFKPK(Projects,deleted," = "," AND") */
        Projects.idFullName = deleted.idFullName
    )
    BEGIN
      SELECT @errno  = 30001,
             @errmsg = 'Cannot delete Personal because Projects exists.'
      GOTO error
    END


    /* erwin Builtin Trigger */
    RETURN
error:
   RAISERROR (@errmsg, -- Message text.
              @severity, -- Severity (0~25).
              @state) -- State (0~255).
    rollback transaction
END

go


CREATE TRIGGER tU_Personal ON Personal FOR UPDATE AS
/* erwin Builtin Trigger */
/* UPDATE trigger on Personal */
BEGIN
  DECLARE  @numrows int,
           @nullcnt int,
           @validcnt int,
           @insidFullName integer,
           @errno   int,
           @severity int,
           @state    int,
           @errmsg  varchar(255)

  SELECT @numrows = @@rowcount
  /* erwin Builtin Trigger */
  /* Personal  Projects on parent update no action */
  /* ERWIN_RELATION:CHECKSUM="00012867", PARENT_OWNER="", PARENT_TABLE="Personal"
    CHILD_OWNER="", CHILD_TABLE="Projects"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_4", FK_COLUMNS="idFullName" */
  IF
    /* %ParentPK(" OR",UPDATE) */
    UPDATE(idFullName)
  BEGIN
    IF EXISTS (
      SELECT * FROM deleted,Projects
      WHERE
        /*  %JoinFKPK(Projects,deleted," = "," AND") */
        Projects.idFullName = deleted.idFullName
    )
    BEGIN
      SELECT @errno  = 30005,
             @errmsg = 'Cannot update Personal because Projects exists.'
      GOTO error
    END
  END


  /* erwin Builtin Trigger */
  RETURN
error:
   RAISERROR (@errmsg, -- Message text.
              @severity, -- Severity (0~25).
              @state) -- State (0~255).
    rollback transaction
END

go




CREATE TRIGGER tD_Project_Status ON Project_Status FOR DELETE AS
/* erwin Builtin Trigger */
/* DELETE trigger on Project_Status */
BEGIN
  DECLARE  @errno   int,
           @severity int,
           @state    int,
           @errmsg  varchar(255)
    /* erwin Builtin Trigger */
    /* Projects  Project_Status on child delete no action */
    /* ERWIN_RELATION:CHECKSUM="0001668c", PARENT_OWNER="", PARENT_TABLE="Projects"
    CHILD_OWNER="", CHILD_TABLE="Project_Status"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_5", FK_COLUMNS="idProjectName" */
    IF EXISTS (SELECT * FROM deleted,Projects
      WHERE
        /* %JoinFKPK(deleted,Projects," = "," AND") */
        deleted.idProjectName = Projects.idProjectName AND
        NOT EXISTS (
          SELECT * FROM Project_Status
          WHERE
            /* %JoinFKPK(Project_Status,Projects," = "," AND") */
            Project_Status.idProjectName = Projects.idProjectName
        )
    )
    BEGIN
      SELECT @errno  = 30010,
             @errmsg = 'Cannot delete last Project_Status because Projects exists.'
      GOTO error
    END


    /* erwin Builtin Trigger */
    RETURN
error:
   RAISERROR (@errmsg, -- Message text.
              @severity, -- Severity (0~25).
              @state) -- State (0~255).
    rollback transaction
END

go


CREATE TRIGGER tU_Project_Status ON Project_Status FOR UPDATE AS
/* erwin Builtin Trigger */
/* UPDATE trigger on Project_Status */
BEGIN
  DECLARE  @numrows int,
           @nullcnt int,
           @validcnt int,
           
           @errno   int,
           @severity int,
           @state    int,
           @errmsg  varchar(255)

  SELECT @numrows = @@rowcount
  /* erwin Builtin Trigger */
  /* Projects  Project_Status on child update no action */
  /* ERWIN_RELATION:CHECKSUM="000198cb", PARENT_OWNER="", PARENT_TABLE="Projects"
    CHILD_OWNER="", CHILD_TABLE="Project_Status"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_5", FK_COLUMNS="idProjectName" */
  IF
    /* %ChildFK(" OR",UPDATE) */
    UPDATE(idProjectName)
  BEGIN
    SELECT @nullcnt = 0
    SELECT @validcnt = count(*)
      FROM inserted,Projects
        WHERE
          /* %JoinFKPK(inserted,Projects) */
          inserted.idProjectName = Projects.idProjectName
    /* %NotnullFK(inserted," IS NULL","select @nullcnt = count(*) from inserted where"," AND") */
    select @nullcnt = count(*) from inserted where
      inserted.idProjectName IS NULL
    IF @validcnt + @nullcnt != @numrows
    BEGIN
      SELECT @errno  = 30007,
             @errmsg = 'Cannot update Project_Status because Projects does not exist.'
      GOTO error
    END
  END


  /* erwin Builtin Trigger */
  RETURN
error:
   RAISERROR (@errmsg, -- Message text.
              @severity, -- Severity (0~25).
              @state) -- State (0~255).
    rollback transaction
END

go




CREATE TRIGGER tD_Projects ON Projects FOR DELETE AS
/* erwin Builtin Trigger */
/* DELETE trigger on Projects */
BEGIN
  DECLARE  @errno   int,
           @severity int,
           @state    int,
           @errmsg  varchar(255)
    /* erwin Builtin Trigger */
    /* Projects  Time_Specification on parent delete no action */
    /* ERWIN_RELATION:CHECKSUM="0004450d", PARENT_OWNER="", PARENT_TABLE="Projects"
    CHILD_OWNER="", CHILD_TABLE="Time_Specification"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_9", FK_COLUMNS="idProjectName" */
    IF EXISTS (
      SELECT * FROM deleted,Time_Specification
      WHERE
        /*  %JoinFKPK(Time_Specification,deleted," = "," AND") */
        Time_Specification.idProjectName = deleted.idProjectName
    )
    BEGIN
      SELECT @errno  = 30001,
             @errmsg = 'Cannot delete Projects because Time_Specification exists.'
      GOTO error
    END

    /* erwin Builtin Trigger */
    /* Projects  Deadlines on parent delete no action */
    /* ERWIN_RELATION:CHECKSUM="00000000", PARENT_OWNER="", PARENT_TABLE="Projects"
    CHILD_OWNER="", CHILD_TABLE="Deadlines"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_6", FK_COLUMNS="idProjectName" */
    IF EXISTS (
      SELECT * FROM deleted,Deadlines
      WHERE
        /*  %JoinFKPK(Deadlines,deleted," = "," AND") */
        Deadlines.idProjectName = deleted.idProjectName
    )
    BEGIN
      SELECT @errno  = 30001,
             @errmsg = 'Cannot delete Projects because Deadlines exists.'
      GOTO error
    END

    /* erwin Builtin Trigger */
    /* Projects  Project_Status on parent delete no action */
    /* ERWIN_RELATION:CHECKSUM="00000000", PARENT_OWNER="", PARENT_TABLE="Projects"
    CHILD_OWNER="", CHILD_TABLE="Project_Status"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_5", FK_COLUMNS="idProjectName" */
    IF EXISTS (
      SELECT * FROM deleted,Project_Status
      WHERE
        /*  %JoinFKPK(Project_Status,deleted," = "," AND") */
        Project_Status.idProjectName = deleted.idProjectName
    )
    BEGIN
      SELECT @errno  = 30001,
             @errmsg = 'Cannot delete Projects because Project_Status exists.'
      GOTO error
    END

    /* erwin Builtin Trigger */
    /* Personal  Projects on child delete no action */
    /* ERWIN_RELATION:CHECKSUM="00000000", PARENT_OWNER="", PARENT_TABLE="Personal"
    CHILD_OWNER="", CHILD_TABLE="Projects"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_4", FK_COLUMNS="idFullName" */
    IF EXISTS (SELECT * FROM deleted,Personal
      WHERE
        /* %JoinFKPK(deleted,Personal," = "," AND") */
        deleted.idFullName = Personal.idFullName AND
        NOT EXISTS (
          SELECT * FROM Projects
          WHERE
            /* %JoinFKPK(Projects,Personal," = "," AND") */
            Projects.idFullName = Personal.idFullName
        )
    )
    BEGIN
      SELECT @errno  = 30010,
             @errmsg = 'Cannot delete last Projects because Personal exists.'
      GOTO error
    END


    /* erwin Builtin Trigger */
    RETURN
error:
   RAISERROR (@errmsg, -- Message text.
              @severity, -- Severity (0~25).
              @state) -- State (0~255).
    rollback transaction
END

go


CREATE TRIGGER tU_Projects ON Projects FOR UPDATE AS
/* erwin Builtin Trigger */
/* UPDATE trigger on Projects */
BEGIN
  DECLARE  @numrows int,
           @nullcnt int,
           @validcnt int,
           @insidProjectName integer,
           @errno   int,
           @severity int,
           @state    int,
           @errmsg  varchar(255)

  SELECT @numrows = @@rowcount
  /* erwin Builtin Trigger */
  /* Projects  Time_Specification on parent update no action */
  /* ERWIN_RELATION:CHECKSUM="0004e71f", PARENT_OWNER="", PARENT_TABLE="Projects"
    CHILD_OWNER="", CHILD_TABLE="Time_Specification"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_9", FK_COLUMNS="idProjectName" */
  IF
    /* %ParentPK(" OR",UPDATE) */
    UPDATE(idProjectName)
  BEGIN
    IF EXISTS (
      SELECT * FROM deleted,Time_Specification
      WHERE
        /*  %JoinFKPK(Time_Specification,deleted," = "," AND") */
        Time_Specification.idProjectName = deleted.idProjectName
    )
    BEGIN
      SELECT @errno  = 30005,
             @errmsg = 'Cannot update Projects because Time_Specification exists.'
      GOTO error
    END
  END

  /* erwin Builtin Trigger */
  /* Projects  Deadlines on parent update no action */
  /* ERWIN_RELATION:CHECKSUM="00000000", PARENT_OWNER="", PARENT_TABLE="Projects"
    CHILD_OWNER="", CHILD_TABLE="Deadlines"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_6", FK_COLUMNS="idProjectName" */
  IF
    /* %ParentPK(" OR",UPDATE) */
    UPDATE(idProjectName)
  BEGIN
    IF EXISTS (
      SELECT * FROM deleted,Deadlines
      WHERE
        /*  %JoinFKPK(Deadlines,deleted," = "," AND") */
        Deadlines.idProjectName = deleted.idProjectName
    )
    BEGIN
      SELECT @errno  = 30005,
             @errmsg = 'Cannot update Projects because Deadlines exists.'
      GOTO error
    END
  END

  /* erwin Builtin Trigger */
  /* Projects  Project_Status on parent update no action */
  /* ERWIN_RELATION:CHECKSUM="00000000", PARENT_OWNER="", PARENT_TABLE="Projects"
    CHILD_OWNER="", CHILD_TABLE="Project_Status"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_5", FK_COLUMNS="idProjectName" */
  IF
    /* %ParentPK(" OR",UPDATE) */
    UPDATE(idProjectName)
  BEGIN
    IF EXISTS (
      SELECT * FROM deleted,Project_Status
      WHERE
        /*  %JoinFKPK(Project_Status,deleted," = "," AND") */
        Project_Status.idProjectName = deleted.idProjectName
    )
    BEGIN
      SELECT @errno  = 30005,
             @errmsg = 'Cannot update Projects because Project_Status exists.'
      GOTO error
    END
  END

  /* erwin Builtin Trigger */
  /* Personal  Projects on child update no action */
  /* ERWIN_RELATION:CHECKSUM="00000000", PARENT_OWNER="", PARENT_TABLE="Personal"
    CHILD_OWNER="", CHILD_TABLE="Projects"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_4", FK_COLUMNS="idFullName" */
  IF
    /* %ChildFK(" OR",UPDATE) */
    UPDATE(idFullName)
  BEGIN
    SELECT @nullcnt = 0
    SELECT @validcnt = count(*)
      FROM inserted,Personal
        WHERE
          /* %JoinFKPK(inserted,Personal) */
          inserted.idFullName = Personal.idFullName
    /* %NotnullFK(inserted," IS NULL","select @nullcnt = count(*) from inserted where"," AND") */
    select @nullcnt = count(*) from inserted where
      inserted.idFullName IS NULL
    IF @validcnt + @nullcnt != @numrows
    BEGIN
      SELECT @errno  = 30007,
             @errmsg = 'Cannot update Projects because Personal does not exist.'
      GOTO error
    END
  END


  /* erwin Builtin Trigger */
  RETURN
error:
   RAISERROR (@errmsg, -- Message text.
              @severity, -- Severity (0~25).
              @state) -- State (0~255).
    rollback transaction
END

go




CREATE TRIGGER tD_Time_Specification ON Time_Specification FOR DELETE AS
/* erwin Builtin Trigger */
/* DELETE trigger on Time_Specification */
BEGIN
  DECLARE  @errno   int,
           @severity int,
           @state    int,
           @errmsg  varchar(255)
    /* erwin Builtin Trigger */
    /* Time_Specification  JobDone on parent delete no action */
    /* ERWIN_RELATION:CHECKSUM="00026027", PARENT_OWNER="", PARENT_TABLE="Time_Specification"
    CHILD_OWNER="", CHILD_TABLE="JobDone"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_8", FK_COLUMNS="idWork" */
    IF EXISTS (
      SELECT * FROM deleted,JobDone
      WHERE
        /*  %JoinFKPK(JobDone,deleted," = "," AND") */
        JobDone.idWork = deleted.idWork
    )
    BEGIN
      SELECT @errno  = 30001,
             @errmsg = 'Cannot delete Time_Specification because JobDone exists.'
      GOTO error
    END

    /* erwin Builtin Trigger */
    /* Projects  Time_Specification on child delete no action */
    /* ERWIN_RELATION:CHECKSUM="00000000", PARENT_OWNER="", PARENT_TABLE="Projects"
    CHILD_OWNER="", CHILD_TABLE="Time_Specification"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_9", FK_COLUMNS="idProjectName" */
    IF EXISTS (SELECT * FROM deleted,Projects
      WHERE
        /* %JoinFKPK(deleted,Projects," = "," AND") */
        deleted.idProjectName = Projects.idProjectName AND
        NOT EXISTS (
          SELECT * FROM Time_Specification
          WHERE
            /* %JoinFKPK(Time_Specification,Projects," = "," AND") */
            Time_Specification.idProjectName = Projects.idProjectName
        )
    )
    BEGIN
      SELECT @errno  = 30010,
             @errmsg = 'Cannot delete last Time_Specification because Projects exists.'
      GOTO error
    END


    /* erwin Builtin Trigger */
    RETURN
error:
   RAISERROR (@errmsg, -- Message text.
              @severity, -- Severity (0~25).
              @state) -- State (0~255).
    rollback transaction
END

go


CREATE TRIGGER tU_Time_Specification ON Time_Specification FOR UPDATE AS
/* erwin Builtin Trigger */
/* UPDATE trigger on Time_Specification */
BEGIN
  DECLARE  @numrows int,
           @nullcnt int,
           @validcnt int,
           @insidWork integer,
           @errno   int,
           @severity int,
           @state    int,
           @errmsg  varchar(255)

  SELECT @numrows = @@rowcount
  /* erwin Builtin Trigger */
  /* Time_Specification  JobDone on parent update no action */
  /* ERWIN_RELATION:CHECKSUM="0002b06d", PARENT_OWNER="", PARENT_TABLE="Time_Specification"
    CHILD_OWNER="", CHILD_TABLE="JobDone"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_8", FK_COLUMNS="idWork" */
  IF
    /* %ParentPK(" OR",UPDATE) */
    UPDATE(idWork)
  BEGIN
    IF EXISTS (
      SELECT * FROM deleted,JobDone
      WHERE
        /*  %JoinFKPK(JobDone,deleted," = "," AND") */
        JobDone.idWork = deleted.idWork
    )
    BEGIN
      SELECT @errno  = 30005,
             @errmsg = 'Cannot update Time_Specification because JobDone exists.'
      GOTO error
    END
  END

  /* erwin Builtin Trigger */
  /* Projects  Time_Specification on child update no action */
  /* ERWIN_RELATION:CHECKSUM="00000000", PARENT_OWNER="", PARENT_TABLE="Projects"
    CHILD_OWNER="", CHILD_TABLE="Time_Specification"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_9", FK_COLUMNS="idProjectName" */
  IF
    /* %ChildFK(" OR",UPDATE) */
    UPDATE(idProjectName)
  BEGIN
    SELECT @nullcnt = 0
    SELECT @validcnt = count(*)
      FROM inserted,Projects
        WHERE
          /* %JoinFKPK(inserted,Projects) */
          inserted.idProjectName = Projects.idProjectName
    /* %NotnullFK(inserted," IS NULL","select @nullcnt = count(*) from inserted where"," AND") */
    select @nullcnt = count(*) from inserted where
      inserted.idProjectName IS NULL
    IF @validcnt + @nullcnt != @numrows
    BEGIN
      SELECT @errno  = 30007,
             @errmsg = 'Cannot update Time_Specification because Projects does not exist.'
      GOTO error
    END
  END


  /* erwin Builtin Trigger */
  RETURN
error:
   RAISERROR (@errmsg, -- Message text.
              @severity, -- Severity (0~25).
              @state) -- State (0~255).
    rollback transaction
END

go


