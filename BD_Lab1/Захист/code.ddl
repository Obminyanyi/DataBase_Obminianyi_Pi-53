
CREATE TABLE [Flights]
( 
	[Date]               datetime  NULL ,
	[Departure_Time]     datetime  NULL ,
	[Arrival_Time]       datetime  NULL ,
	[Destination]        varchar(20)  NULL ,
	[id_Planes]          integer  NULL 
)
go

CREATE TABLE [Plane]
( 
	[Airplan_Brand]      varchar(20)  NULL ,
	[Number_Plane]       integer  NULL ,
	[Crew_Count]         integer  NULL ,
	[id_Planes]          integer  NOT NULL 
)
go

ALTER TABLE [Plane]
	ADD CONSTRAINT [XPKPlane] PRIMARY KEY  CLUSTERED ([id_Planes] ASC)
go


ALTER TABLE [Flights]
	ADD CONSTRAINT [R_1] FOREIGN KEY ([id_Planes]) REFERENCES [Plane]([id_Planes])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go


CREATE TRIGGER tD_Flights ON Flights FOR DELETE AS
/* erwin Builtin Trigger */
/* DELETE trigger on Flights */
BEGIN
  DECLARE  @errno   int,
           @severity int,
           @state    int,
           @errmsg  varchar(255)
    /* erwin Builtin Trigger */
    /* Plane  Flights on child delete no action */
    /* ERWIN_RELATION:CHECKSUM="0001360b", PARENT_OWNER="", PARENT_TABLE="Plane"
    CHILD_OWNER="", CHILD_TABLE="Flights"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_1", FK_COLUMNS="id_Planes" */
    IF EXISTS (SELECT * FROM deleted,Plane
      WHERE
        /* %JoinFKPK(deleted,Plane," = "," AND") */
        deleted.id_Planes = Plane.id_Planes AND
        NOT EXISTS (
          SELECT * FROM Flights
          WHERE
            /* %JoinFKPK(Flights,Plane," = "," AND") */
            Flights.id_Planes = Plane.id_Planes
        )
    )
    BEGIN
      SELECT @errno  = 30010,
             @errmsg = 'Cannot delete last Flights because Plane exists.'
      GOTO error
    END


    /* erwin Builtin Trigger */
    RETURN
error:
   RAISERROR (@errmsg, -- Message text.
              @severity, -- Severity (0~25).
              @state) -- State (0~255).
    rollback transaction
END

go


CREATE TRIGGER tU_Flights ON Flights FOR UPDATE AS
/* erwin Builtin Trigger */
/* UPDATE trigger on Flights */
BEGIN
  DECLARE  @numrows int,
           @nullcnt int,
           @validcnt int,
           
           @errno   int,
           @severity int,
           @state    int,
           @errmsg  varchar(255)

  SELECT @numrows = @@rowcount
  /* erwin Builtin Trigger */
  /* Plane  Flights on child update no action */
  /* ERWIN_RELATION:CHECKSUM="00018729", PARENT_OWNER="", PARENT_TABLE="Plane"
    CHILD_OWNER="", CHILD_TABLE="Flights"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_1", FK_COLUMNS="id_Planes" */
  IF
    /* %ChildFK(" OR",UPDATE) */
    UPDATE(id_Planes)
  BEGIN
    SELECT @nullcnt = 0
    SELECT @validcnt = count(*)
      FROM inserted,Plane
        WHERE
          /* %JoinFKPK(inserted,Plane) */
          inserted.id_Planes = Plane.id_Planes
    /* %NotnullFK(inserted," IS NULL","select @nullcnt = count(*) from inserted where"," AND") */
    select @nullcnt = count(*) from inserted where
      inserted.id_Planes IS NULL
    IF @validcnt + @nullcnt != @numrows
    BEGIN
      SELECT @errno  = 30007,
             @errmsg = 'Cannot update Flights because Plane does not exist.'
      GOTO error
    END
  END


  /* erwin Builtin Trigger */
  RETURN
error:
   RAISERROR (@errmsg, -- Message text.
              @severity, -- Severity (0~25).
              @state) -- State (0~255).
    rollback transaction
END

go




CREATE TRIGGER tD_Plane ON Plane FOR DELETE AS
/* erwin Builtin Trigger */
/* DELETE trigger on Plane */
BEGIN
  DECLARE  @errno   int,
           @severity int,
           @state    int,
           @errmsg  varchar(255)
    /* erwin Builtin Trigger */
    /* Plane  Flights on parent delete no action */
    /* ERWIN_RELATION:CHECKSUM="0000ff19", PARENT_OWNER="", PARENT_TABLE="Plane"
    CHILD_OWNER="", CHILD_TABLE="Flights"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_1", FK_COLUMNS="id_Planes" */
    IF EXISTS (
      SELECT * FROM deleted,Flights
      WHERE
        /*  %JoinFKPK(Flights,deleted," = "," AND") */
        Flights.id_Planes = deleted.id_Planes
    )
    BEGIN
      SELECT @errno  = 30001,
             @errmsg = 'Cannot delete Plane because Flights exists.'
      GOTO error
    END


    /* erwin Builtin Trigger */
    RETURN
error:
   RAISERROR (@errmsg, -- Message text.
              @severity, -- Severity (0~25).
              @state) -- State (0~255).
    rollback transaction
END

go


CREATE TRIGGER tU_Plane ON Plane FOR UPDATE AS
/* erwin Builtin Trigger */
/* UPDATE trigger on Plane */
BEGIN
  DECLARE  @numrows int,
           @nullcnt int,
           @validcnt int,
           @insid_Planes integer,
           @errno   int,
           @severity int,
           @state    int,
           @errmsg  varchar(255)

  SELECT @numrows = @@rowcount
  /* erwin Builtin Trigger */
  /* Plane  Flights on parent update no action */
  /* ERWIN_RELATION:CHECKSUM="00011de0", PARENT_OWNER="", PARENT_TABLE="Plane"
    CHILD_OWNER="", CHILD_TABLE="Flights"
    P2C_VERB_PHRASE="", C2P_VERB_PHRASE="", 
    FK_CONSTRAINT="R_1", FK_COLUMNS="id_Planes" */
  IF
    /* %ParentPK(" OR",UPDATE) */
    UPDATE(id_Planes)
  BEGIN
    IF EXISTS (
      SELECT * FROM deleted,Flights
      WHERE
        /*  %JoinFKPK(Flights,deleted," = "," AND") */
        Flights.id_Planes = deleted.id_Planes
    )
    BEGIN
      SELECT @errno  = 30005,
             @errmsg = 'Cannot update Plane because Flights exists.'
      GOTO error
    END
  END


  /* erwin Builtin Trigger */
  RETURN
error:
   RAISERROR (@errmsg, -- Message text.
              @severity, -- Severity (0~25).
              @state) -- State (0~255).
    rollback transaction
END

go


