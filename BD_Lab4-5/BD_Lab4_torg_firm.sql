--Task1
create view qresult as
select zakaz_tovar.id_zakaz, klient.Nazva, zakaz_tovar.id_tovar,zakaz_tovar.Kilkist, 
zakaz_tovar.Znigka, tovar.Price*zakaz_tovar.Kilkist*(1-zakaz_tovar.Znigka) as Zagalna_vartist
from (klient INNER JOIN zakaz ON klient.id_klient = zakaz.id_klient)
INNER JOIN (tovar INNER JOIN zakaz_tovar ON tovar.id_tovar = zakaz_tovar.id_tovar)
ON zakaz.id_zakaz = zakaz_tovar.id_zakaz;

--Task2
create view qresult2 as
select qresult.id_zakaz, klient.Nazva, zakaz.date_naznach, 
sum(qresult.Zagalna_vartist) as Pidsumok
from (klient INNER JOIN zakaz ON klient.id_klient = zakaz.id_klient)
INNER JOIN qresult ON zakaz.id_zakaz = qresult.id_zakaz
group by qresult.id_zakaz, klient.Nazva, zakaz.date_naznach;

--Task3
select sotrudnik.Fname, sotrudnik.Name, sotrudnik.Posada, 
AVG(qresult2.Pidsumok) as [Avg-Pidsumok]
from (sotrudnik INNER JOIN zakaz ON sotrudnik.id_sotrud = zakaz.id_sotrud)
INNER JOIN qresult2 ON zakaz.id_zakaz = qresult2.id_zakaz
group by sotrudnik.Fname, sotrudnik.Name, sotrudnik.Posada;

--Task4
select sotrudnik.Fname, sotrudnik.Name,sotrudnik.Posada,
AVG(QResult2.Pidsumok) as [Avg-Pidsumok]
from (sotrudnik join zakaz on sotrudnik.id_sotrud=zakaz.id_sotrud)
join QResult2 on zakaz.id_zakaz=QResult2.id_zakaz
group by sotrudnik.Fname,sotrudnik.Name,sotrudnik.Posada;

--Task5
select klient.Nazva, sum(QResult.Zagalna_vartist) as [sum-Zagagalna_vartist]
from(klient join zakaz on klient.id_klient=zakaz.id_klient) join
QResult on zakaz.id_zakaz=QResult.id_zakaz
group by klient.Nazva

--Task6
select Tovar.Nazva, Max((Tovar.Price*zakaz_tovar.Kilkist*(1-zakaz_tovar.Znigka)))AS Zagalna_vartist
from tovar inner join zakaz_tovar on tovar.id_tovar = zakaz_tovar.id_tovar
group by Tovar.Nazva

--Task7
select sotrudnik.Fname, sotrudnik.Name, sotrudnik.Posada,
Count(zakaz.id_zakaz) AS [Count-id_zakaz]
from zakaz INNER JOIN sotrudnik ON zakaz.id_sotrud = sotrudnik.id_sotrud
group by sotrudnik.Fname, sotrudnik.Name, sotrudnik.Posada;


--1
SELECT Klient.Nazva, Postachalnik.Nazva
FROM Klient, Postachalnik
WHERE Klient.City = Postachalnik.City;

--2
SELECT Tovar.Nazva
FROM Tovar
WHERE (Tovar.Price>(SELECT AVG (Price)
FROM Tovar));

--3
SELECT *
FROM Tovar
WHERE id_postav IN (SELECT id_tovar FROM Zakaz_tovar 
WHERE Znigka >= 0.05);

--4
set identity_insert tovar on
INSERT INTO Tovar(id_tovar, Nazva, Price, id_postav)
VALUES (25, '�����', 12.5, 1);

--5
INSERT INTO Klient(nazva, id_klient)
SELECT Nazva, id_postach
FROM Postachalnik
WHERE Postachalnik.city='�������';

--6
UPDATE Tovar SET Price = (1.13*Price)
WHERE id_postav=1;

--7
DELETE 
FROM Tovar
WHERE id_tovar=25;

--8
DELETE 
FROM Zakaz
WHERE id_klient=(SELECT id_klient FROM Klient 
WHERE Nazva = [������ ����� �볺���]);

--9
SELECT Postachalnik.Nazva, Postachalnik.City, Postachalnik.Adress
FROM Postachalnik
UNION 
SELECT Klient.Nazva, Klient.City, Klient.Adress
FROM Klient;

--10
Declare @min decimal(10,4) = 0.5, @date_begin DateTime = '01.01.2018'
SELECT zakaz_tovar.id_zakaz, zakaz.date_rozm, tovar.price, zakaz_tovar.kilkist
FROM zakaz INNER JOIN (Tovar INNER JOIN zakaz_tovar 
ON Tovar.id_tovar=zakaz_tovar.id_tovar) 
ON zakaz.id_zakaz=zakaz_tovar.id_zakaz
WHERE tovar.price>@min And zakaz.date_rozm>=@date_begin;


--1) ������� �������� ������� ������ �� ���������.
select SUM(tovar.NaSklade) as zagalna_kilk_tov 
from tovar

--2) ������� �������� ������� ����������� ����������.
select count(sotrudnik.id_sotrud) as zagalna_kilk_sotrudnik
from sotrudnik



--3) ������� �������� ������� ������������� ����������.
select count(postachalnik.id_postach) as zagalna_kilk_posta4alnik
from postachalnik

SELECT postachalnik.id_postach as KilkPostach
FROM   postachalnik
WHERE  id_postach=(SELECT MAX(id_postach) FROM postachalnik) 

--4) ������� ������� �� ������ �������, �� �������� � ��������� �����.

select tovar.Nazva, sum(zakaz_tovar.Kilkist)
from tovar  join zakaz_tovar on tovar.id_tovar= zakaz_tovar.id_tovar
join zakaz on zakaz.id_zakaz= zakaz_tovar.id_zakaz
where zakaz.date_naznach between '2017-01-01' and '2017-12-31'
group by tovar.Nazva

--5) ������� ����, �� ��� ���� �������� ������ � ��������� �����.
select tovar.Nazva, sum(tovar.Price) zag_summ
from tovar join zakaz_tovar on tovar.id_tovar = tovar.Price
join zakaz on zakaz.id_zakaz= zakaz_tovar.id_zakaz
where zakaz.date_naznach between '2017-01-01' and '2017-12-31'
group by tovar.Nazva



--6) ������� ���� ������� ������ �� ������ ��������������.
select postachalnik.Nazva, SUM(tovar.Price) as suma_prodazhu
from postachalnik join tovar on tovar.id_postav = postachalnik.id_postach
group by postachalnik.Nazva

--7) ������� �������� ������� ��������� �� ������ ��������������, �� ����� ������.
select postachalnik.Nazva, count(tovar.NaSklade) as kilk_zamovlen
from postachalnik join tovar on tovar.id_postav = postachalnik.id_postach
where tovar.Nazva like '������'
group by postachalnik.Nazva

--8) ������� ������� ����, �� ��� ���������� �����.
select zakaz_tovar.id_tovar, avg(zakaz_tovar.Kilkist*tovar.price) as serednya_suma
from zakaz_tovar join tovar on tovar.id_tovar = zakaz_tovar.id_tovar
group by zakaz_tovar.id_tovar


--9) ������� ������� ��������� ��� �볺���, �� �������� � �������.
select klient.Nazva, sum(tovar.price) as vartist_zamov
from tovar join zakaz_tovar on tovar.id_tovar = zakaz_tovar.id_tovar
join zakaz on zakaz.id_zakaz = zakaz_tovar.id_zakaz
join klient on zakaz.id_klient = klient.id_klient
where klient.City like '�������'
group by klient.Nazva

--10) ������� ������� ���� �� ������ �� ������� �������������.
select postachalnik.Nazva, tovar.Nazva, AVG(tovar.Price) as serednya_cina
from postachalnik join tovar on tovar.id_postav = postachalnik.id_postach
group by postachalnik.Nazva, tovar.Nazva

















