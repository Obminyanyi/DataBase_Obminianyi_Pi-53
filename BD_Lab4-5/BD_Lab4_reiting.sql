﻿--1) Сумарний рейтинг студента з кожної дисципліни.
select dbo_student.Name_ini, predmet.Nazva, sum(Reiting.Reiting) as sumarnyi_reiting
from dbo_student
join Reiting on dbo_student.Kod_stud = Reiting.Kod_student
join Rozklad_pids on Reiting.K_zapis = Rozklad_pids.K_zapis
join predmet on Rozklad_pids.K_predm_pl = predmet.K_predmet
group by dbo_student.Name_ini, predmet.Nazva

--2) Розрахувати кількість студентів у кожній групі.
select dbo_groups.Kod_group, dbo_groups.kilk
from dbo_groups

--3) Розрахувати кількість дисциплін за групою. ??????
select dbo_groups.Kod_group, count(Predmet_plan.K_predmet) as kilk_predmet
from dbo_groups join Predmet_plan on dbo_groups.K_navch_plan = Predmet_plan.K_navch_plan
group by dbo_groups.Kod_group



--4) Розрахувати кількість проведених занять у кожній групі. ??????
select dbo_groups.Kod_group, count(Predmet_plan.K_predmet) 
from dbo_groups join Predmet_plan on dbo_groups.K_navch_plan = Predmet_plan.K_navch_plan
group by dbo_groups.Kod_group

--5) Розрахувати середній бал за групою. ??????
select dbo_groups.Kod_group, AVG(Reiting.Reiting) as serednyi_bal
from dbo_groups 
join dbo_student on dbo_groups.Kod_group = dbo_student.Kod_group
join Reiting on dbo_student.Kod_stud = Reiting.Kod_student
group by dbo_groups.Kod_group

--6) Розрахувати середній бал з дисципліни.
select predmet.Nazva, AVG(Reiting.Reiting) as serednyi_bal
from Reiting
join Rozklad_pids on Reiting.K_zapis = Rozklad_pids.K_zapis
join predmet on Rozklad_pids.K_predm_pl = predmet.K_predmet
group by predmet.Nazva

--7) Розрахувати поточний рейтинг студента з кожної дисципліни.
select dbo_student.Name_ini, predmet.Nazva, Reiting.Reiting
from dbo_student 
join Reiting on dbo_student.Kod_stud = Reiting.Kod_student
join Rozklad_pids on Reiting.K_zapis = Rozklad_pids.K_zapis
join predmet on Rozklad_pids.K_predm_pl = predmet.K_predmet

--8) Відобразити найменший рейтинг студентів з дисципліни.
select dbo_student.Name_ini, predmet.Nazva, min(Reiting.Reiting) as min_reiting
from dbo_student
join Reiting on dbo_student.Kod_stud = Reiting.Kod_student
join Rozklad_pids on Reiting.K_zapis = Rozklad_pids.K_zapis
join predmet on Rozklad_pids.K_predm_pl = predmet.K_predmet
group by dbo_student.Name_ini, predmet.Nazva

--9) Відобразити найбільший студентський рейтинг з дисципліни. ??????
select dbo_student.Name_ini, predmet.Nazva, max(Reiting.Reiting) as min_reiting
from dbo_student
join Reiting on dbo_student.Kod_stud = Reiting.Kod_student
join Rozklad_pids on Reiting.K_zapis = Rozklad_pids.K_zapis
join predmet on Rozklad_pids.K_predm_pl = predmet.K_predmet
group by dbo_student.Name_ini, predmet.Nazva

--10) Розрахувати кількість проведених занять за видами для кожної дисципліни.

--11) Розрахувати кількість груп за кожною спеціальністю.
select Spetsialnost.Nazva, count(dbo_groups.Kod_group) as kilk_groups
from Spetsialnost 
join Navch_plan on Spetsialnost.K_spets = Navch_plan.K_spets
join Predmet_plan on Navch_plan.K_navch_plan = Navch_plan.K_navch_plan
join predmet on Predmet_plan.K_predmet = predmet.K_predmet
join Rozklad_pids on Rozklad_pids.K_predm_pl = predmet.K_predmet
join dbo_groups on Rozklad_pids.Kod_group = dbo_groups.Kod_group
group by Spetsialnost.Nazva

