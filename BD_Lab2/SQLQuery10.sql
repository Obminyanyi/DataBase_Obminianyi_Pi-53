CREATE DATABASE myDataBase


create table Personal
(idFullName int IDENTITY(1,1) not null primary key,
FullName varchar(50),
EmploymentTheProject varchar(50),
) ;

create table Time_Specification
(idWork int IDENTITY(1,1) not null primary key,
WorkName varchar(20),
Specification date,
TimeCost numeric(10)
) ;

create table Projects
(idProjectName int IDENTITY(1,1) not null primary key,
ProjectName varchar(20),
Cost numeric(20),
ExpirationDate date,
) ;

create table Project_Status
(idStatus int IDENTITY(1,1) not null primary key,
P_Status varchar(20),
idProjectName int not null foreign key (idProjectName) references dbo.Projects(idProjectName) on delete cascade on update no action,
Stage varchar(20),
) ;

create table Job_Done
(idJobDone int IDENTITY(1,1) not null primary key,
idProjectName int not null foreign key (idProjectName) references dbo.Projects(idProjectName) on delete cascade on update no action,
idFullName int not null foreign key (idFullName) references dbo.Personal(idFullName) on delete cascade on update no action,
idWork int not null foreign key (idWork) references dbo.Time_Specification(idWork) on delete cascade on update no action,
DateOfWork date,
) ;

insert into Personal(FullName, EmploymentTheProject)
values ('��������� ������� ����������', '��������'),
('������� ��������� ����������', '�������� �������'),
('������� ��������� �����������', '�����������'),
('�������� ���� ���������', '�����������'),
('�������� ���� ����������', '�����������'),
('������� ����� �����������', '�����������'),
('������� ��� ����������', '�����������'),
('�������� ����� �����������', '�����������');

select * from Personal;

insert into Time_Specification(WorkName, Specification, TimeCost)
values ('CreateDB', '2018-04-21',250 ),
 ('UpdateLocalFiles', '2018-04-20',50 ),
 ('CreateNewServer', '2018-06-10',200 ),
 ('DeleteOldVerssions', '2018-05-01',30 ),
 ('AddNewLevel', '2018-06-15',60 ),
 ('FixBugs', '2018-07-26',150 );

 select * from Time_Specification;

 insert into Projects(ProjectName,Cost, ExpirationDate)
values ('Tast', 100,'2018-04-15'),
('Sail',1150,'2018-04-15'),
('Injured',5000,'2018-05-08'),
('Emergency',3200,'2018-05-03'),
('Safely',1500,'2018-07-19'),
('Interruption',2500,'2018-04-01'),
('Knitted',2750,'2018-04-23'),
('Publicity',1300,'2018-06-02'),
('Excite',1190,'2018-07-10'),
('Candy',3550,'2018-06-27');


 

 insert into Project_status(P_Status,idProjectName, Stage)
values ('In developing',1, '����������'),
('Done',2,'���������'),
('In developing',3,'������������ '),
('Done',4,'���������'),
('In developing',5,'������'),
('In developing',6,'��������������'),
('Done',7,'���������'),
('In developing',8,'������������'),
('Done',9,'���������'),
('In developing',10,'��������������');


select * from Project_status;


 insert into Job_Done(idProjectName,idFullName,idWork, DateOfWork)
values (1,1,1, '2018-04-17' ),
(2,2,2, '2018-04-16'),
(3,3,3, '2018-05-10' ),
(4,4,4, '2018-05-15');


select * from Job_Done;



